Versatile: Prototype
---

This is project is a prototype for a decentralized, P2P, modular and back-endless
social network build on top of IPFS. It is based on the post that I previously
wrote named [*General guesses and hypothesis about decentralized social networks*](https://kaisarasar.wordpress.com/2018/09/17/general-guesses-and-hypothesis-about-decentralized-social-networks/)
and extends the proof of concept that I already wrote on [python](https://gitlab.com/CesarACabrera/versatile-proof-of-concept).

The main objective of this prototype is to proof, to a bigger extent than the
previous proof of concept, the viability, at least in principle, of the project.
For more information about how does it work please read the original post, where
I explain everything in detail. Some of the problems mentioned there were solved
in this prototype.

You can access the site [here](https://cesaracabrera.gitlab.io/versatile-prototype/#/).

**Acknoledgments**

* To everyone that works on the amazing projects that are IPFS and Libp2p.
* To Marijn Haverbeke for his amazing book on JavaScript.
* To The Net Ninja, because his tutorials helped me a lot to learn the technologies I used.
* To LIDSOL (Laboratorio de Investigación y Desarollo de Software Libre) for helping me with advices and infrastructure.
* To Xavier from Simply Explained for his tutorials about blockchain in JavaScript.
* There is definetely other people which knowledge and code helped me and that I am forgetting. To all of them I apologize and big thanks!
