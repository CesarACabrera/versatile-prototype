# Order of the elements in this project
---

**The order of the JavaScript logic goes as follows:**

1. Imports go by alphabetic order (brackets does not alter the order). If the import follows some format it does not alter the order.
1. Variables go by order.
1. The order of the store and the elements goes by priority, not by alphabethical order.
1. Registration of the components also goes by order.
1. Functions and computed properties also go by order.
1. CSS also goes by alphabetical order.
1. Cyclehoods go in life order.
1. Props also go by alphabetical order.
1. The order of the arguments in the functions goes in order of use inside of the function.

**The order inside the HTML elements goes as follows:**

1. Id.
1. Vuetify specific props, then slots and then events of the element, all of them  by alphabetical order.
1. HTML attributes by alphabetical order, except for those that are about positioning or have something to do with CSS.
1. Vue logic, all of them sorted by alphabetical order of their variables and constituents:
    * **a)** V-logic (v-if, v-for, etc.) by alphabethical order.
    * **b)** V-slots.
    * **d)** V-model.
    * **e)** V-bind.
    * **f)** Vue props.
    * **g)** Vue events.
1. HTML attributes that are about positioning or have something to do with CSS by alphabetical order.
1. Vuetify general attributes by alphabetical order, except for "color".
1. Grid attributes from smaller to larger.
1. Vuetify "class" attribute.
1. Vuetify "color" attribute.

Use color over class when possible.
Directives do not use abbriviations.

**The order inside the class atributes goes as follows:**

1. Everything goes by alphabetical order. Colors that need special properties are exceptions.
1. When possible, vertical margin should be handled by element involved, either by margin top or my, not by margin of a different element.
1. The same for horizontal margin.

---
*I used JSDoc syntax for documenting this project. However, JSDoc is not installed.*
