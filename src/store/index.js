/*

Copyright (c) 2020 Cesar Cabrera

This file is part of Versatile: Prototype.

Versatile: Prototype is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

Versatile: Prototype is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Versatile: Prototype.  If not, see <https://www.gnu.org/licenses/>.

*/

import { Block, History } from "../history.js";
const Buffer = require('buffer/').Buffer;
import { bus } from "../main.js";
import crypto from "crypto";
import eccrypto from "eccrypto";
import IPFS from "ipfs";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    actualKey: null,
    buffer: {},
    /**
    * Generates a SHA-256 hash.
    * @param {string} string - The string to be hashed.
    * @returns {string} - The SHA-256 hash of the string.
    */
    generateHash: (string) => {
      return crypto.createHash("sha256").update(string).digest();
    },
    history: new History(),
    /**
    * Creates a new block in the blockchain.
    * @param {Object} state - The Vuex state.
    * @param {Object} payload - The data of block to be added and if is or not encrypted.
    * @returns {Object} - The signature of the block's hash.
    */
    newBlock: async (state, payload) => {
      let date = new Date();

      if (payload.encrypted !== false) {
        let encrypted = await eccrypto.encrypt(Buffer.from(state.toArray(payload.encrypted)), Buffer.from(JSON.stringify(payload.data)));
        payload.data = JSON.stringify(encrypted);
      }

      let newBlock = new Block(
        state.history.chain.length,
        date.toString(),
        state.history.chain[state.history.chain.length - 1].hash,
        payload.data,
        payload.encrypted
      )

      let signature = await eccrypto.sign(state.privateKey, state.toArray(newBlock.hash));
      newBlock["signature"] = signature.toString('hex');

      state.history.chain.push(newBlock);

      bus.$emit('userDataChange', {});

      return signature;
    },
    node: IPFS.create({
      repo: `ipfs/versatile/${Math.random()}`,
      config: {
          Bootstrap: [
            "/dns4/ams-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLer265NRgSp2LA3dPaeykiS1J6DifTC88f5uVQKNAd",
            "/dns4/lon-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLMeWqB7YGVLJN3pNLQpmmEk35v6wYtsMGLzSr5QBU3",
            "/dns4/nyc-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLueR4xBeUbY9WZ9xGUUxunbKWcrNFTDAadQJmocnWm",
            "/dns4/nyc-2.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLV4Bbm51jM9C4gDYZQ9Cy3U6aXMJDAbzgu2fzaDs64"
          ],
          Addresses: {
            Swarm: [
              "/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star/"
            ]
          }
        }
    }),
    privateKey: null,
    /**
    * Creates the history for the new friend.
    * Adds the friend to the friends list.
    * Generates the topic to be subscribed to (friend's public key hash).
    * Defines the response for when the friend publishes something new.
    * Subscribes to the topic.
    * @param {Object} state - The Vuex state.
    * @param {string} friendKey - Friend's public key from a QR code.
    * @param {boolean} push - Defines if you want the new friend to be pushed to the friends list.
    */
    processCode: async (state, friendKey, push) => {
      let node = await state.node;

      state.buffer[friendKey] = {
        hash: null,
        holding: new History()
      }

      if (push === true) {
        state.history.friends.push({
          name: "New friend",
          key: friendKey
        });
      }

      let topic = state.generateHash(friendKey).toString('hex');

      /**
      * Gets the published hash. Then retrieves the history, checks if any
      * publication is encrypted and, if it is the case, decrypts it.
      * After al this prcess proceeds to pass the history to the buffer.
      * @param {Object} resp - The response from the topic.
      */
      let response = async (resp) => {
        let hash = resp.data.toString();
        let retrievedHistory = await state.retrieveIPFS(state, hash);
        let holding = JSON.parse(retrievedHistory);

        let verified = [];
        let encryptedPublications = [];
        let promises = [];

        /** Once everything has been checked and, if it is the case, decrypted,
        * passes the decrypted history to the buffer.
        */
        function passHoldingToBuffer() {
          holding.chain = verified;

          if (state.buffer.hasOwnProperty(`${friendKey}`) === true) {
            state.buffer[friendKey].hash = hash;
            state.buffer[friendKey].holding = holding;
          }

          state.history.friends.forEach(item => {
            if (item.key === friendKey) {
              item.name = holding.profile.name;
            }
          });

          bus.$emit('friendPublished', {
            key: friendKey,
            text: `${holding.profile.name} published to his history`,
            color: "black"
          });

          bus.$emit("userDataChange", {});
        }

        /** Decrypts any encrypted publication. */
        function checkEncryptionBeforeBuffer() {

          if (encryptedPublications.length > 0) {
            Promise.all(promises)
              .then(decrypted => {
                encryptedPublications.forEach((publication, index) => {
                  publication.data = JSON.parse(decrypted[index].toString());
                });

                passHoldingToBuffer();
              })
              .catch(error => {
                throw error;
              });
          } else {
            passHoldingToBuffer();
          }
        }

        /** Checks if there is any encrypted publication for the user in the history. */
        function verifyEncryption() {
          verified.forEach((publication, index, array) => {
            if (publication.encrypted !== false && publication.encrypted === state.publicKey.toString('hex')) {
              encryptedPublications.push(publication);

              try {
                let parsed = JSON.parse(publication.data);

                let toDecrypt = {};

                /* This is done to set the prototype of the encrypted data to Uint8Array(65),
                so it can be properly decrypted. */
                Object.keys(parsed).forEach(key => {
                  toDecrypt[key] = Uint8Array.from(parsed[key].data);
                  toDecrypt[key].__proto__ = state.publicKey.__proto__;
                });

                promises.push(eccrypto.decrypt(Buffer.from(state.toArray(state.privateKey.toString('hex'))), toDecrypt));
              } catch (error) {
                throw error;
              }

            }

            if (index === array.length - 1) {
              checkEncryptionBeforeBuffer();
            }
          });
        }

        state.verifyChain(state, holding, friendKey, verified, verifyEncryption, []);

      };

      await node.pubsub.subscribe(topic, response);

      bus.$emit('processFinished', {
        text:  `Friend successfuly added! \nYou must wait until he publishes something new to see his publications.`,
        color: "success"
      });
    },
    publicKey: null,
    /**
    * Publish content to a PubSub topic.
    * @param {Object} state - The Vuex state.
    * @param {string} topic - The topic where to publish the content.
    * @param {string} content - Content to publish.
    */
    publish: async (state, topic, content) => {
      let node = await state.node;
      await node.pubsub.publish(topic, content);
    },
    /**
    * Reads, uploads and sets an image as profile avatar.
    * @param {Object} state - The Vuex state.
    * @param {Object} file - The image to set as an avatar.
    */
    readFile: (state, file) => {
      const reader = new window.FileReader();

      reader.readAsArrayBuffer(file);
      reader.onloadend = async () => {
        let hash = await state.uploadIPFS(state, reader.result);
        state.history.profile.avatar = hash;
        bus.$emit("userDataChange", {});
      };
    },
    /**
    * Retrieves a file from IPFS.
    * @param {Object} state - The Vuex state.
    * @param {string} hash - CID of the file to be retrieved.
    * @returns {Object} - The file retrieved from IPFS.
    */
    retrieveIPFS: async (state, hash) => {
      let node = await state.node;
      const chunks = [];
      for await (const chunk of node.cat(hash)) {
        chunks.push(chunk);
      }

      let chunksBuffer = Buffer.concat(chunks);

      return chunksBuffer;
    },
    tabs: null,
    /**
    * Create an Uint8Array from a hexadecimal string.
    * @param {string} hexString - The hexadecimal string to be converted.
    * @returns {Uint8Array(32)} - The Uint8Array.
    */
    toArray: (hexString) => {
      return new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
    },
    /**
    * Uploads any content to IPFS.
    * @param {Object} state - The Vuex state.
    * @param {string} content - Content to be uploaded.
    * @returns {string} - The CID of the uploaded file.
    */
    uploadIPFS: async (state, content) => {
        let node = await state.node;

        let file = {
          content: Buffer.from(content)
        }

        let filesAdded = await node.add(file);

        return filesAdded.cid.string;
    },
    /**
    * Verifies the signature of every publication in the blockchain.
    * @param {Object} state - The Vuex state.
    * @param {Object} history - Friend's history.
    * @param {string} key - Friend's public key.
    * @param {Array} verified - Array where the verified publications will be pushed.
    * @param {function} callback - Function to call after the verification of the blockchain.
    * @param {Array} args - Array of arguments to be passed to the callback function.
    */
    verifyChain: (state, history, key, verified, callback, args) => {
      history.chain.forEach((publication, index, array) => {
        eccrypto.verify(state.toArray(key),
          state.generateHash(publication.index + publication.timestamp + publication.previousHash + publication.data),
          state.toArray(publication.signature))
          .then(() => {

            if (index === 0) {
              verified.push(publication);
            } else if (publication.previousHash === verified[index - 1].hash) {
              verified.push(publication);
            } else {
              // console.error("The previous hash does not match.");
            }

            if (index === array.length - 1) {
              callback(...args)
            }
          });
      });
    }
  },
  mutations: {
    /**
    * Uploads the history of the user to IPFS.
    * @param {Object} state - The Vuex state.
    */
    post: async (state) => {

      try {
        let historyToUpload = JSON.parse(JSON.stringify(state.history));
        let encrypted = await eccrypto.encrypt(Buffer.from(state.publicKey), Buffer.from(JSON.stringify(state.history.friends)));
        historyToUpload.friends = JSON.stringify(encrypted);

        let hash = await state.uploadIPFS(state, JSON.stringify(historyToUpload));

        state.publish(state, state.generateHash(state.publicKey.toString('hex')).toString('hex'), hash);
        state.buffer[state.publicKey.toString('hex')].hash = hash;
      } catch (error) {
        throw "Unable to upload history: " + error;
      }

    },
    processCode: (state, payload) => {
      state.processCode(state, payload.content, payload.push);
    },
    readFile: (state, payload) => {
      state.readFile(state, payload);
    },
    retrieve: (state, hash) => {
      state.retrieveIPFS(state, hash);
    },
    /**
    * Updates the image URL source of a class of elements.
    * @param {Object} state - The Vuex state.
    * @param {Object} payload - An object that contains the CID of the image to be retrieved and the class of elements to aply it.
    */
    updateImage: async (state, payload) => {
      let retrievedFile = await state.retrieveIPFS(state, payload.avatarHash);

      let blob = new Blob( [retrievedFile], { type: "image/jpeg" } );

      let img = document.getElementsByClassName(payload.class);

      img.forEach(element => {
        element.src = URL.createObjectURL(blob);
      });
    },
    uploadIPFS: (state, payload) => {
      state.uploadIPFS(state, ...payload);
    },
  },
  actions: {
    post: (context) => {
      context.commit("post");
    },
    processCode: (context, payload) => {
      context.commit("processCode", payload);
    },
    readFile: (context, payload) => {
      context.commit("readFile", payload);
    },
    retrieve: (context, hash) => {
      context.commit("retrieve", hash);
    },
    uploadIPFS: (context, payload) => {
      context.commit("uploadIPFS", payload)
    }
  }
});
