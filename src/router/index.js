/*

Copyright (c) 2020 Cesar Cabrera

This file is part of Versatile: Prototype.

Versatile: Prototype is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

Versatile: Prototype is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Versatile: Prototype.  If not, see <https://www.gnu.org/licenses/>.

*/

import Vue from 'vue'
import VueRouter from 'vue-router'
import MainPage from '../views/MainPage.vue'
import CreateAccount from '../views/CreateAccount.vue'
import Template from '../views/Template.vue'
import VueQrcodeReader from "vue-qrcode-reader";

Vue.use(VueRouter)
Vue.use(VueQrcodeReader)

const routes = [
  {path: "/", component: MainPage},
  {path: "/create-account", component: CreateAccount},
  {path:"/template", component: Template}
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
