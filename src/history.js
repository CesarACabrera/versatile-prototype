
/*

Copyright (c) 2020 Cesar Cabrera

This file is part of Versatile: Prototype.

Versatile: Prototype is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

Versatile: Prototype is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Versatile: Prototype.  If not, see <https://www.gnu.org/licenses/>.

*/

/**
* Contains the classes that generates the history and the blocks in the blockchain.
* @module history
*/

import crypto from "crypto";

/** Class that generates a new block in the blockchain. */
class Block {
  /**
  * @param {number} index - Index of the publication.
  * @param {string} timestamp - Date when te publication wasa created.
  * @param {string} previousHash - Hash of the previous publication.
  * @param {Object} data - Data of the publication.
  * @param {boolean|string} encrypted - Key of the friend towards the publication is directed. 'False' otherways.
  */
  constructor(index, timestamp, previousHash = "", data, encrypted) {
    this.index = index;
    this.timestamp = timestamp;
    this.previousHash = previousHash;
    this.data = data;
    this.encrypted = encrypted;
    this.hash = this.calculateHash();
  }

  /** Generates the SHA-256 hash of the publication. */
  calculateHash() {
    return crypto.createHash("sha256").update(this.index + this.timestamp + this.previousHash + this.data).digest().toString('hex');
  }
}

/** Class that generates a new history. */
class History {
  constructor() {
    this.chain = [];
    this.friends = [];
    this.profile = {
      name: "New user",
      bio: "Write a brief story of yourself!",
      birthday: null,
      location: "Where do you reside?",
      avatar: null
    };
    this.extensions = [{
        category: "Chat",
        categoryExtensions: [{
          name: "Orbit: Decentralized IPFS chat.",
          url: "https://orbit.chat/#/connect"
        }]
      }];
  }
  /**
  * Generates the first block in the blockchain.
  * @param {Object} - User's private key.
  */
  createGenesisBlock(privateKey) {
    let date = new Date();
    this.chain.push(new Block("0", date.toString(), crypto.createHash("sha256").update(privateKey.toString('hex')).digest().toString('hex'), {
      text: "Welcome to Versatile!",
      file: null,
      tags: null
    }, false));
  }
}

export { Block, History };
